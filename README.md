# SpeakScript

This script allows you to turn a text file into an audio file of spoken text.
It uses Coqui-TTS to "speak" the input text file.

# Dependencies and Installation

```console
sudo apt-get install ffmpeg sox libsox-fmt-mp3 python3 python3-pip mkvtoolnix llvm-10 pdftoppm
pip3 install TTS
sudo cpan -i Digest::MD5
```

You can also install it in a virtual environment:

```console
sudo apt-get install ffmpeg sox libsox-fmt-mp3 python3 python3-venv python3-pip mkvtoolnix llvm-10 pdftoppm
python3 -m venv venv
. venv/bin/activate
pip install wheel
export LLVM_CONFIG=`which llvm-config-10`
pip install TTS
```

Beware: Even if it uses a venv, some files are put in $HOME/.local/share/tts !

# Usage

```console
bash speak.sh txtfile.txt spokentext.mp3
```

If you want to add your `spokentext.mp3` to a video file (e. g. a screencast you have created),
you can use the script `add-audio-to-screen-cast.sh`:

```console
bash ./add-audio-to-screen-cast.sh screencast-without-audio.ogv spokentext.mp3 screencast-with-audio.ogv
```

You can also use the perl-script `speak.pl` to create a slideshow. Check `txtfile2.txt` and `test.mp4`
for how this looks like. Call it with:

```
perl speak.pl --input=txtfile2.txt --output=test.mp4 --debug
```

You can create the required images from a PDF by using a command similar to:
```
pdftoppm -png -scale-to-x 3840 -scale-to-y -1 some.pdf image
```

This creates files `image-01.png`, `image-02.png`, etc.

# Tips

Short breaks are added to the output, if your textfile includes text lines similar to:

```
# silence 5
```

In this case, the number represents the duration of silence (in seconds).

This displays 1.jpg (when using an mp4 as output):

```
# image 1.jpg
```

You can also use single pages from pdfs:

```
# page 1 of some.pdf
```

Any other appearances of `#` in a line will result in the whole line being ignored.

Sometimes, text contains abbreviations that are not pronounced correctly. Please
add the correct pronounciation to the file `pronounciations`. This file is
automatically applied. Verify it with the following command:

```
perl speak.pl --input=example-pronounciations.txt --output=example-pronounciations.mp3
```
