#!/bin/bash

#use with:
# ./speak.sh <input-file> <output-file>

input_file=$1
output_file=$2
cachedir=tmp

if ! type -t deactivate > /dev/null && [ -f venv/bin/activate ]
then
	echo reading env
	. venv/bin/activate
fi

if [ ! -d $cachedir ]
then
        mkdir $cachedir
fi

parts=()

while IFS=$'\n' read -u 9 line
do
	if [[ $line =~ "# silence " ]]
	then
		SEC=`echo $line | sed -e 's/.*silence //i'`
		thisfile="$cachedir/silence_$SEC.mp3"
		if [ ! -e "$thisfile" ]
		then
			ffmpeg -f lavfi -i anullsrc=r=22050:cl=mono -t $SEC -q:a 9 -acodec libmp3lame "$thisfile"
		fi
		parts+=( "$thisfile" )
	elif [[ $line =~ "#" ]]
	then
		echo "Skipping $line"
	else
		thisfile="$cachedir/`echo $line | md5sum | sed -e 's/\s*-.*//'`.mp3"
		if [ ! -e "$thisfile" ]
		then
			tts --out_path "$thisfile" --text "$line"
		fi
		if [ ! -e "$thisfile.mp3" ]
		then
			ffmpeg -i "$thisfile" -af "silenceremove=start_periods=1:start_duration=1:start_threshold=-60dB:detection=peak,aformat=dblp,areverse,silenceremove=start_periods=1:start_duration=1:start_threshold=-60dB:detection=peak,aformat=dblp,areverse" "$thisfile.mp3"
		fi
		if [ -e "$thisfile.mp3" ]
		then
			parts+=( "$thisfile.mp3" )
		else
			echo "FAILED to cut silence!!!"
			parts+=( "$thisfile" )
		fi
	fi
done 9< "$input_file"

sox ${parts[*]} "$output_file"
