#!/usr/bin/perl

use utf8;
use strict;
use warnings;
use Data::Dumper;
use Digest::MD5 qw/md5_hex/;
use File::Copy 'move';

my %options = (
	input => undef,
	output => undef,
	debug => 0,
	videosize => "1024x768"
);

sub debug (@) {
	if($options{debug}) {
		foreach (@_) {
			warn "$_\n";
		}
	}
}

analyze_args(@ARGV);

main();

sub main {
	debug "main()";
	die "--output not specified" unless $options{output};
	die "--input not specified" unless $options{input};

	die "$options{input} does not exist" unless -e $options{input};

	mkdir "tmp" unless -d "tmp";

	my @structure = parse_input_file();
	if ($options{output} =~ m#\.mp4$#) {
		if(contains_image_or_video(@structure)) {
			if(exists $structure[0]->{image} || exists $structure[0]->{video}) {
				my @audio_snippets = create_audio_snippets(@structure);
				my %new_structure = get_sound_between_images_or_videos(@structure);
				my %audio_files = ();
				foreach my $imgname (keys %new_structure) {
					my @files = map { + { ($_->{text} ? $_->{text} : 'silence') => ($_->{text} ? "tmp/".md5_hex($_->{text}).".mp3" : "tmp/silence_$_->{silence}.mp3" ) } } grep { exists $_->{text} || exists $_->{silence} } @{$new_structure{$imgname}};
					my $this_filename = "tmp/".md5_hex(join(",", @files)).".mp3";
					$audio_files{$imgname} = merge_audio_files($this_filename, @files);
				}

				my @video_snippets = ();
				foreach my $i (0 .. $#structure) {
					if(exists $structure[$i]->{image}) {
						my $imgname = $structure[$i]->{image};
						if(-e $imgname) {
							my $audio_filename = $audio_files{$imgname};
							if (!-e $audio_filename) {
								die "$audio_filename not found";
							}
							my $video_filename = "tmp/".md5_hex($imgname).".mp4";
							unlink $video_filename;
							if(!-e $video_filename) {
								my $command = qq#ffmpeg -loop 1 -i $imgname -i $audio_filename -c:a copy -c:v libx264 -shortest $video_filename#;
								debug $command;
								system($command);
								exit $? if $?;
							}

							if(!-e $video_filename) {
								die "$video_filename could not be found";
							} else {
								push @video_snippets, $video_filename;
							}
						} else {
							die "$imgname not found";
						}
					}
				}
				unlink $options{output};
				if(!-e $options{output}) {
					my $first = $video_snippets[0];
					my $number_of_snippets = @video_snippets;
					my @rest = @video_snippets[1 .. $#video_snippets];
					my $rest_code = "-i " . $first;
					if(@rest) {
						$rest_code .= " -i ".join(" -i ", @rest);
					}
					my $command = qq#ffmpeg $rest_code -filter_complex "concat=n=$number_of_snippets:v=1:a=1 [v] [a]" -map "[v]" -map "[a]" $options{output}#;
					debug $command;
					system $command;
					exit $? if $?;
				}
			} else {
				die "First line must contain image/video if you create a video";
			}
		} else {
			die "Cannot create mp4 file without image/video";
		}
	} else {
		my @audio_snippets = create_audio_snippets(@structure);
		debug "Output is mp3. Images and videos will be ignored";
		merge_audio_files($options{output}, @audio_snippets);
	}
}

sub get_sound_between_images_or_videos {
	my @structure = @_;

	my %new_structure = ();

	my $current_image = undef;
	foreach my $i (0 .. $#structure) {
		if($structure[$i]->{image}) {
			$current_image = $structure[$i]->{image};
		} elsif($structure[$i]->{video}) {
			$current_image = $structure[$i]->{video};
		} else {
			if($current_image) {
				push @{$new_structure{$current_image}}, $structure[$i];
			} else {
				die "Must start with image";
			}
		}
	}
	return %new_structure;
}

sub merge_audio_files {
	my $output = shift;
	return $output if -e $output;

	my @audio_snippets = @_;

	my @files = ();
	foreach my $item (@audio_snippets) {
		foreach (keys %{$item}) {
			push @files, $item->{$_};
		}
	}

	my $command = "sox ".join(" ", @files)." ".$output;
	debug $command;
	system($command);
	exit $? if $?;
	return $output;
}

sub translate_abbreviations {
	my %pronounciations = map { chomp; $_ } map { split /=/ } qx(cat pronounciations);
	exit $? if $?;
	my $line = shift;
	my @output = ();
	foreach my $key (keys %pronounciations) {
		$line =~ s#\b$key\b#$pronounciations{$key}#gi;
	}
	debug $line;
	return $line;
}

sub create_audio_snippets {
	my @structure = @_;

	my @files = ();

	foreach (@structure) {
		if(exists $_->{text}) {
			push @files, { $_->{line} => tts($_->{text}) };
		} elsif (exists $_->{silence}) {
			push @files, { $_->{line} => silence($_->{silence}) };
		}
	}

	return @files;
}

sub contains_image_or_video {
	my @structure = @_;

	foreach (@structure) {
		if(exists $_->{image} || exists $_->{video}) {
			return 1;
		}
	}
	return 0;
}

sub parse_input_file {
	debug "parse_input_file -> $options{input}";
	open my $fh, '<', $options{input};

	my @structure = ();

	while (my $line = <$fh>) {
		chomp $line;
		$line =~ s#"##g;
		if($line !~ m#^\s*$#) {
			if($line =~ m/^\s*#(.*)/) {
				my $comment = $1;
				if($comment =~ m/\s*silence (\d+)/) {
					push @structure, { silence => $1, line => $line };
				} elsif($comment =~ m/\s*image (.*)/) {
					my $file = "tmp/$1.resized$options{videosize}.jpg";
					qx(convert "$1" -resize $options{videosize}\\! "$file");
					exit $? if $?;
					push @structure, { image => $file, line => $line };
				} elsif($comment =~ m/\s*video (.*)/) {
					push @structure, { video => $1, line => $line };
				} elsif($comment =~ m/\s*page (\d+) of (.*\.pdf)/) {
					my $file = "tmp/$2.page_$1.$options{videosize}.png.jpg";
					if(!-e $file) {
						qx(pdftoppm -png -scale-to-x 3840 -scale-to-y -1 -f "$1" -singlefile "$2" "tmp/$2.page_$1");
						exit $? if $?;
						qx(convert "tmp/$2.page_$1.png" -resize $options{videosize}\\! "$file");
						exit $? if $?;
					}
					push @structure, { image => $file, line => $line };
				} else {
					push @structure, { comment => $line, line => $line };
				}
			} else {
				if($line =~ m/(.*?)\s*#\s*(.*)/) {
					push @structure, { text => translate_abbreviations($1), line => $line };
					push @structure, { comment => $2, line => $line };
				} else {
					push @structure, { text => translate_abbreviations($line), line => $line };
				}
			}
		}
	}
	close $fh;

	return @structure;
}

sub silence {
	my $secs = shift;
	my $thisfile = "tmp/silence_${secs}.mp3";
	if (!-e $thisfile) {
		system(qq#ffmpeg -f lavfi -i anullsrc=r=22050:cl=mono -t $secs -q:a 9 -acodec libmp3lame $thisfile#);
		exit $? if $?;
	}
	return $thisfile;
}

sub tts {
	my $sentence = shift;
	my $thisfile = "tmp/".md5_hex($sentence).".mp3";
	if (!-e $thisfile) {
		my $command = qq#tts --out_path "$thisfile" --text "$sentence"#;
		debug $command;
		system($command);
		exit $? if $?;

=head
		my $no_silence = qq#ffmpeg -i $thisfile -af "silenceremove=start_periods=1:start_duration=1:start_threshold=-60dB:detection=peak,aformat=dblp,areverse,silenceremove=start_periods=1:start_duration=1:start_threshold=-60dB:detection=peak,aformat=dblp,areverse" ${thisfile}_nosilence.mp3#;
		debug $no_silence;
		system($no_silence);
		exit $? if $?;
		move $thisfile, "$thisfile.original";
		move "${thisfile}_nosilence.mp3", $thisfile;
=cut
	}
	return $thisfile;
}

sub _help {
	my $exit = shift;
	print <<EOF;
perl speak.pl --input=file.txt --output=file.mp3

Parameters:

--help					This help
--debug					Enables debug-mode
--input=file.txt			Uses file.txt as input-file
--output=file.mp3			Uses file.mp3 as output-file
--videosize=1048x768			Size of the video
EOF

	exit $exit;
}

sub analyze_args {
	foreach (@_) {
		if(/^--debug$/) {
			$options{debug} = 1;
		} elsif (/^--help/) {
			_help(0);
		} elsif (/^--input=(.*)/) {
			$options{input} = $1;
		} elsif (/^--videosize=(\d+x\d+)/) {
			$options{videosize} = $1;
		} elsif (/^--output=(.*)$/) {
			$options{output} = $1;
			if($options{output} !~ m#\.mp[34]$#) {
				warn "File ending not specified";
			}
		} else {
			warn "ERROR: Invalid parameter $_\n";
			_help(1);
		}
	}
}
