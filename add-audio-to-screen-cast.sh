#!/bin/bash
input_video=$1
input_audio=$2
output_video=$3
ffmpeg -i "$input_video" -i "$input_audio" -c:v copy -map 0:v:0 -map 1:a:0 -c:a libvorbis "$output_video"

